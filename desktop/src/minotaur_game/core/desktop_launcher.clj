(ns minotaur-game.core.desktop-launcher
  (:require [minotaur-game.core :refer :all])
  (:import [com.badlogic.gdx.backends.lwjgl LwjglApplication]
           [org.lwjgl.input Keyboard])
  (:gen-class))

(defn -main
  []
  (LwjglApplication. minotaur-game-game "Minotaur Game" 600 600)
  (Keyboard/enableRepeatEvents true))
