(ns minotaur-game.minotaur
  (:require [minotaur-game.maze :as maze]))

(defn sign [a]
  "Takes a number, and returns it's sign,
so 55 => 1, -23 => -1, 0 => 0"
  (if (zero? a)
    0 (if (< a 0)
        -1 1)))

(defn can-see [maze-grid minotaur x y]
  "Checks if the minotaur can see (x, y) in the maze
by tracing rays from it's position to (x, y)"
  (let [width (count (first maze-grid))
        height (count maze-grid)
        ;; find the ray direction
        minotaur-x (:x minotaur)
        minotaur-y (:y minotaur)
        dx (sign (- x minotaur-x))
        dy (sign (- y minotaur-y))]
    ;; trace the ray
    (loop [curr-x minotaur-x
           curr-y minotaur-y]
      (if-not (maze/valid-coord? curr-x curr-y width height)
        false
        (if (and (= curr-x x) (= curr-y y))
          true
          (if (:obstructed (maze/grid-val maze-grid curr-x curr-y))
            false
            (recur (+ curr-x dx) (+ curr-y dy))))))))

(defn close-path [maze-grid minotaur player-x player-y min-dist max-dist]
  (let [[target-x target-y]
        (loop [[x y] [player-x player-y]]
          ;; find a point to go to, near the player
          (let [dist (maze/eucldist x y player-x player-y)]
            (if (and (<= min-dist dist) (< dist max-dist))
              [x y]
              (recur (maze/random-empty-coord maze-grid
                                              :start-x (max 0 (- player-x max-dist))
                                              :start-y (max 0 (- player-y max-dist))
                                              :end-x (min (- (count (first maze-grid)) 1)
                                                          (+ player-x max-dist))
                                              :end-y (min (- (count maze-grid) 1)
                                                          (+ player-y max-dist)))))))]
    (maze/A* maze-grid (:x minotaur) (:y minotaur) target-x target-y)))

(defn wandering-path [maze-grid minotaur player-x player-y]
  "Returns a path fxor the minotaur to wander on."
  (if (<= (rand) 0.6)
    (let [[x y] (maze/random-empty-coord maze-grid)]
      (maze/A* maze-grid (:x minotaur) (:y minotaur) x y))
    (close-path maze-grid minotaur player-x player-y 16 25)))

(defn can-attack [maze minotaur player-x player-y curr-state]
  (or (can-see maze minotaur player-x player-y)
      (<= (maze/BFS-distance maze
                             (:x minotaur) (:y minotaur)
                             player-x player-y)
          ;; if the minotaur is already attacking, it can track the player better
          (if (= :attacking curr-state) 6 3))))

(defn update-state [maze minotaur player-x player-y]
  (let [dist (maze/eucldist (:x minotaur) (:y minotaur) player-x player-y)]
    (case (:state minotaur)
      :wandering
      ;; is the player close to the minotaur?
      (if (<= dist 10)
        (assoc minotaur
               :state :close
               :path (close-path maze minotaur player-x player-y 2 8))
        minotaur)
      :close
      ;; is the player still close, or should we go to wandering or attacking?
      (let [attackable (can-attack maze minotaur player-x player-y :close)]
        (if (and (<= dist 10)
                 (not attackable))
          minotaur
          (if attackable
            (assoc minotaur
                   :state :attacking
                   :path (maze/A* maze (:x minotaur) (:y minotaur) player-x player-y))
            (assoc minotaur
                   :state :wandering
                   :path (wandering-path maze minotaur player-x player-y)))))
      :attacking
      ;; is the player still visible, or should we go to close mode
      (if (can-attack maze minotaur player-x player-y :attacking)
        minotaur
        (assoc minotaur
               :state :close
               :path (close-path maze minotaur player-x player-y 2 8))))))

(defn process [maze minotaur player-x player-y]
  (update-state
   maze
   (if-not (or (nil? (:path minotaur)) (empty? (:path minotaur)))
     ;; continue on the current path
     (let [[x y] (first (:path minotaur))]
       (assoc minotaur
              :path (next (:path minotaur))
              :x x :y y))
     ;; make a new path
     (assoc minotaur :path
            (case (:state minotaur)
              :wandering (wandering-path maze minotaur player-x player-y)
              :close (close-path maze minotaur player-x player-y 2 8)
              :attacking (maze/A* maze (:x minotaur) (:y minotaur) player-x player-y)
              nil)))
   player-x player-y))

(defn new-minotaur [maze x y]
  "Returns a new minotaur-information containing map"
  {:x x :y y
   :state :wandering
   :path nil})
