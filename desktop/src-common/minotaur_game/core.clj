(ns minotaur-game.core
  (:require [play-clj.core :refer :all]
            [play-clj.g2d :refer :all]
            [play-clj.ui :refer :all]
            [minotaur-game.maze :as maze]
            [minotaur-game.minotaur :as mt]))

(defn renderable [entities]
  (let [{:keys [player-x player-y minotaur FOV maze collectables win-state]}
        (:config (first entities))
        {:keys [player-texture minotaur-texture block-texture collectable-texture background-texture]}
        (:texture (first entities))
        block-width (quot (game :width) (+ 1 (* 2 FOV)))
        block-height (quot (game :height) (+ 1 (* 2 FOV)))]
    
    (cons
     (assoc
      background-texture
      :x (* (- 0 player-x) block-width)
      :y (* (- 0 player-y) block-height))
     (remove
      nil?
      (for [y (range (- player-y FOV 2) (+ player-y FOV 1))
            x (range (- player-x FOV 2) (+ player-x FOV 1))]
        (when-let [texture
                   (cond
                     (and (= x (:x minotaur)) (= y (:y minotaur))) minotaur-texture
                     (and (= x player-x) (= y player-y)) player-texture
                     (and (maze/valid-coord? maze x y)
                          (:obstructed (maze/grid-val maze x y))) block-texture
                     (contains? collectables [x y]) collectable-texture)]
          (assoc texture
                 :x (* block-width (- x (- player-x FOV)))
                 :y (* block-height (- y (- player-y FOV)))
                 :width block-width :height block-height)))))))

(defn move-player [screen entities dx dy]
  (let [{:keys [maze player-x player-y player-can-move collectables]}
        (:config (first entities))
        new-player-x (+ player-x dx)
        new-player-y (+ player-y dy)]
    ;; can the player move there?
    (cond
      (not player-can-move)
      [(update-in (first entities) [:config :player-move-backlog]
                  (fn [bl] [dx dy]))]
      
      (and (maze/valid-coord? maze new-player-x new-player-y)
           (not (:obstructed (maze/grid-val maze new-player-x new-player-y))))
      ;; move there.
      (let [out (update-in (first entities) [:config]
                           (fn [c] (assoc c
                                          :player-can-move false
                                          :player-x new-player-x
                                          :player-y new-player-y)))
            new-coord [new-player-x new-player-y]]
        (add-timer! screen :player-move-cooldown-over 1/4)
        (if (contains? collectables new-coord)
          (update-in out [:config :collectables]
                     (fn [coll] (clojure.set/difference coll #{new-coord})))
          out))
      
      ;; don't move there
      true entities)))

(defscreen main-screen
  :on-show
  (fn [screen entities]
    (update! screen :renderer (stage))
    (add-timer! screen :process-minotaur 0 1/3)
    (add-timer! screen :check-win 0 1/5)
    
    (println "### Minotaur Game")
    (println "Find and collect all the coins in the maze,")
    (println "and avoid the minotaur to win!")
    
    (let [maze (maze/wrap-maze (maze/add-passthrough (maze/generate 41 41) 6))
          FOV 7
          n-collectables 13
          block-width (quot (game :width) (+ 1 (* 2 FOV)))
          block-height (quot (game :height) (+ 1 (* 2 FOV)))]
      [{:config {:player-x 1
                 :player-y 1
                 :player-can-move true
                 :player-move-backlog nil
                 :win-state nil
                 :minotaur (mt/new-minotaur maze
                                            (- (count (first maze)) 2)
                                            (- (count maze) 2))
                 :collectables
                 (reduce (fn [acc elt]
                           (if (>= (count acc) n-collectables)
                             (reduced acc)
                             (let [coord (maze/random-empty-coord maze)]
                               (if (contains? acc coord)
                                 acc (conj acc coord)))))
                         #{} (range))
                 :FOV FOV
                 :maze maze}
        :texture {:player-texture (texture "player.png")
                  :minotaur-texture (texture "minotaur.png")
                  :block-texture (texture "block/grey.png")
                  :background-texture (assoc (texture "background.png")
                                             :width (* block-width (+ (* 2 FOV) (count (first maze))))
                                             :height (* block-height (+ (* 2 FOV) (count maze))))
                  :collectable-texture (texture "collectable.png")}}]))
  :on-render
  (fn [screen entities]
    (clear!)
    (render! screen (renderable entities))
    entities)
  
  :on-timer
  (fn [screen entities]
    (if (:win-state (:config (first entities)))
      entities
      (case (:id screen)
        :check-win
        (let [{:keys [maze minotaur player-x player-y]}
              (:config (first entities))]
          (cond
            (empty? (:collectables (:config (first entities))))
            (do
              ;; no i can't be bothered to show some sort of text..
              (println "You Won!")
              [(update-in (first entities) [:config :win-state]
                          (fn [ws] :win))])
            (and (= (:x minotaur) player-x) (= (:y minotaur) player-y))
            (do
              ;; no i can't be bothered to show some sort of text..
              (println "You Lost!")
              [(update-in (first entities) [:config :win-state]
                          (fn [ws] :lose))])
            true entities))
        :process-minotaur
        (let [{:keys [maze minotaur player-x player-y]}
              (:config (first entities))]
          [(update-in (first entities) [:config :minotaur]
                      (fn [minotaur] (mt/process maze minotaur player-x player-y)))])
        :player-move-cooldown-over
        (let [out (update-in (first entities) [:config :player-can-move]
                             (fn [v] true))]
          (if-let [[dx dy] (:player-move-backlog (:config out))]
            (move-player screen [(update-in out [:config :player-move-backlog]
                                            (fn [bl] nil))]
                         dx dy) [out]))
        entities)))
  
  :on-key-down
  (fn [screen entities]
    (if (:win-state (:config (first entities)))
      entities
      (cond
        (= (:key screen) (key-code :dpad-up))
        (move-player screen entities 0 1)
        (= (:key screen) (key-code :dpad-down))
        (move-player screen entities 0 -1)
        (= (:key screen) (key-code :dpad-left))
        (move-player screen entities -1 0)
        (= (:key screen) (key-code :dpad-right))
        (move-player screen entities 1 0)
        true entities))))

(defgame minotaur-game-game
  :on-create
  (fn [this]
    (set-screen! this main-screen)))

;; REPL onvenience functions go here
(defn update-screen []
  (play-clj.core/on-gl (set-screen! minotaur-game-game main-screen)))
(defscreen blank-screen
  :on-render
  (fn [screen entities]
    (clear!)))
(play-clj.core/set-screen-wrapper!
 (fn [screen screen-fn]
   (try (screen-fn)
        (catch Exception e
          (.printStackTrace e)
          (play-clj.core/set-screen! minotaur-game-game blank-screen)))))
