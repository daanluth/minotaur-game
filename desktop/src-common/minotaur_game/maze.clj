(ns minotaur-game.maze
  (:require
   [minotaur-game.priorityheap :as ph]))

(defn empty-maze-grid [width height]
  ;; the maze is a vector of vectors, containing the maze cells
  (vec (repeat height
               (vec (repeat width {:obstructed true})))))

(defmacro grid-val [grid x y]
  ;; convenience function
  `((~grid ~y) ~x))

(defn valid-coord?
  ([grid x y] (valid-coord? x y (count (first grid)) (count grid)))
  ([x y width height]
   (and (>= x 0) (>= y 0) (< x width) (< y height))))

(defn random-empty-coord [grid &{:keys [start-x start-y end-x end-y]
                                 :or {start-x 0 start-y 0
                                      end-x (count (first grid))
                                      end-y (count grid)}}]
  ;; finds some random empty coordinate between (start-x start-y) (end-x end-y) (inclusive)
  (let [height (count grid)
        width (count (first grid))
        possible (remove
                  nil?
                  (for [y (range start-y (+ end-y 1))
                        x (range start-x (+ end-x 1))]
                    (when (and (valid-coord? x y width height)
                               (not (:obstructed (grid-val grid x y))))
                      [x y])))]
    (rand-nth possible)))

(defn eucldist [x1 y1 x2 y2]
  "The euclidean distance from (x1, y1) to (x2, y2)"
  (Math/sqrt (+ (Math/pow (- x1 x2) 2)
                (Math/pow (- y1 y2) 2))))

(defn update-maze [grid x y key value]
  ;; update a value in a maze grid
  (update-in grid [y x key] (fn [v] value)))

(defn maze-neighbors [grid x y &{:keys [step]
                                 :or {step 1}}]
  (let [deltas [[(- 0 step) 0] [step 0]
                [0 (- 0 step)] [0 step]]
        height (count grid)
        width (count (first grid))]
    ;; try each possible end coordinate, and remove those that are invalid
    (filter (fn [[x y]] (valid-coord? x y width height))
            (map (fn [[dx dy]] [(+ x dx) (+ y dy)]) deltas))))

;; to wrap a maze in a layer of wall
(defn wrap-maze [grid &{:keys [top bottom left right]
                        :or {top true bottom true left true right true}}]
  (cond
    top (wrap-maze (vec (concat [(into [] (repeat (count (first grid)) {:obstructed true}))] grid))
                   :top false :bottom bottom :left left :right right)
    bottom (wrap-maze (conj grid (into [] (repeat (count (first grid)) {:obstructed true})))
                      :top top :bottom false :left left :right right)
    left (wrap-maze (into [] (map (fn [row] (into [] (concat [{:obstructed true}] row))) grid))
                    :top top :bottom bottom :left false :right right)
    right (wrap-maze (into [] (map (fn [row] (conj row {:obstructed true})) grid))
                     :top top :bottom bottom :left left :right false)
    true grid))

(defn possible-passthrough-points [grid]
  (let [height (count grid)
        width (count (first grid))]
    (filter (fn [[x y]]
              (when (and (grid-val grid x y)
                         (not (= (mod x 2) (mod y 2))))
                (let [top (if (valid-coord? x (+ y 1) width height)
                            (:obstructed (grid-val grid x (+ y 1))) false)
                      down (if (valid-coord? x (- y 1) width height)
                             (:obstructed (grid-val grid x (- y 1))) false)
                      left (if (valid-coord? (- x 1) y width height)
                             (:obstructed (grid-val grid (- x 1) y)) false)
                      right (if (valid-coord? (+ x 1) y width height)
                              (:obstructed (grid-val grid (+ x 1) y)) false)]
                  ;; either one, but not both
                  (or (and (and top down) (not (or left right)))
                      (and (and left right) (not (or top down)))))))
            (for [y (range height) x (range width)] [x y]))))
(defn add-passthrough [grid n]
  (loop [possible-points (possible-passthrough-points grid)
         curr-grid grid
         curr-n n]
    (if (or (empty? possible-points) (<= curr-n 0))
      curr-grid
      (let [[x y] (rand-nth possible-points)]
        (recur (remove (fn [[px py]]
                         (<= (eucldist x y px py) 6))
                       (possible-passthrough-points curr-grid))
               (update-maze curr-grid x y :obstructed false)
               (- curr-n 1))))))

(defn generate [width height]
  (loop [grid (empty-maze-grid width height)
         edge-queue (ph/ph-push ph/NEW [[0 0] [0 0]] 0)]
    
    (if (empty? edge-queue)
      grid
      (let [[[start-x start-y] [end-x end-y]] (ph/ph-peek edge-queue)]
        (if-not (:obstructed (grid-val grid end-x end-y))
          (recur grid (ph/ph-pop edge-queue))
          (let [between-x (+ start-x (quot (- end-x start-x) 2))
                between-y (+ start-y (quot (- end-y start-y) 2))
                new-edges
                (reduce (fn [acc [x y]]
                          (if (:obstructed (grid-val grid x y))
                            (conj acc [[end-x end-y] [x y]]) acc))
                        [] (maze-neighbors grid end-x end-y :step 2))]
            
            ;; update the grid, adding the new edge with a random priority
            (recur
             (-> grid
                 (update-maze start-x start-y :obstructed false)
                 (update-maze between-x between-y :obstructed false)
                 (update-maze end-x end-y :obstructed false))
             (reduce (fn [queue elt]
                       (ph/ph-push queue elt (rand-int 100)))
                     (ph/ph-pop edge-queue)
                     new-edges))))))))

(defn BFS-distance [grid start-x start-y end-x end-y]
  "Finds the Breadth first search distance in the maze, from
(start-x, start-y) to (end-x, end-y), +infinity if there is no path."
  (loop [queue (conj (clojure.lang.PersistentQueue/EMPTY) [start-x start-y 0])
         visited #{}]
    (if (empty? queue) Double/POSITIVE_INFINITY
        (let [[x y dist] (peek queue)
              new-dist (+ dist 1)]
          (if (and (= x end-x) (= y end-y)) dist
              (recur (reduce (fn [acc [end-x end-y]]
                               (if-not (or (contains? visited [end-x end-y])
                                           (:obstructed (grid-val grid end-x end-y)))
                                 (conj acc [end-x end-y new-dist]) acc))
                             (pop queue) (maze-neighbors grid x y))
                     (conj visited [x y])))))))

(defn A* [grid start-x start-y end-x end-y]
  "Finds the shortest path in the maze from (start-x, start-y) to (end-x, end-y),
using A* with the heuristic of euclidean distance. Returns nil if there is no path,
[(x1 y1) (x2 y2) ... (end-x end-y)] otherwise (the start position is not included)."
  (loop [queue (ph/ph-push ph/NEW [start-x start-y 0] 0)
         back {} visited #{}]

    (if (empty? queue)
      nil
      (let [[x y dist] (ph/ph-peek queue)
            new-dist (+ dist 1)]
        (if (and (= x end-x) (= y end-y))
          ;; traverse through the backpointers, back to the start
          (drop 1 (loop [path (cons [x y] nil)]
                    (if-let [next (get back (first path))]
                      (recur (cons next path)) path)))
          ;; move on
          (let [neighbors (remove (fn [elt] (or (contains? visited elt)
                                                (let [[x y] elt] (:obstructed (grid-val grid x y)))))
                                  (maze-neighbors grid x y))]
            (recur (reduce (fn [acc [neighbor-x neighbor-y]]
                             (ph/ph-push acc [neighbor-x neighbor-y new-dist]
                                         (+ new-dist (eucldist neighbor-x neighbor-y end-x end-y))))
                           (ph/ph-pop queue) neighbors)
                   (reduce (fn [acc elt] (assoc acc elt [x y])) back neighbors)
                   (conj visited [x y]))))))))

;; DEBUG
(defn print-maze! [grid]
  (doseq [row grid]
    (doseq [val row]
      (print (if (:obstructed val)
               "#" ".")))
    (println)))
