(ns minotaur-game.priorityheap)

;;; Priority queue (min heap)
(defn heap-upheap
  [heap index index-value]
  ;; compare the node with it's parent
  (let [parent-index (quot (- index 1) 2)]
    (if-let [parent-value (get heap parent-index)]
      (if (< (:priority index-value) (:priority parent-value))
        ;; parent < current swap them and continue up the heap
        (recur (assoc heap
                      parent-index index-value
                      index parent-value)
               parent-index
               index-value)
        ;; if we are at the top, or no swaps are nessecary, return the heap as it is
        heap) heap)))

;; performs downheap on the given priority queue (heap)
(defn heap-downheap
  [heap index index-value]
  ;; compare the node with it's children
  (let [left-child (+ (* 2 index) 1)
        right-child (+ left-child 1)
        left-value (get heap left-child)
        right-value (get heap right-child)]
    ;; if the current node is the smallest, swap it down and continue
    (if (and left-value right-value)
      (if (< (:priority left-value) (:priority right-value))
        ;; left < right
        (if (< (:priority left-value) (:priority index-value))
          ;; left < parent
          (recur (assoc heap
                        left-child index-value
                        index left-value)
                 left-child index-value)
          heap)
        ;; right > left
        (if (< (:priority right-value) (:priority index-value))
          ;; right < parent
          (recur (assoc heap
                        right-child index-value
                        index right-value)
                 right-child index-value)
          heap))
      heap)))

;; to push to the priority queue (heap)
(defn ph-push
  ([heap elt priority]
   (let [new-elt {:value elt :priority priority}]
     (heap-upheap (conj heap new-elt) (count heap) new-elt))))

;; to pop from the priority queue (heap)
(defn ph-pop
  ([heap]
   (let [last-elt (last heap)]
     (heap-downheap (clojure.core/pop (assoc heap 0 last-elt))
                    0 last-elt))))

;; to peek at the priority queue (heap)
(defn ph-peek
  ([heap] (:value (first heap))))
;; to find the priority
(defn ph-peek-priority
  ([heap] (:priority (first heap))))

;; a new priority queue (heap)
(def NEW [])
