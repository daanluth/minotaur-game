# Minotaur
## Introduction

A small play-clj game which is (very) loosely based on  
the story of the [Minotaur](https://wikipedia.org/wiki/Minotaur)  
in the sense that you wander through a maze, and have to avoid a monster.  
There are some coins in the (41x41) maze, and if you collect all 13 of them you win,  
but if you get caught by the minotaur, you lose.  

## Example
![Minotaur Game Demo Image](minotaur_demo.png)

## Getting started
You can either run the the prebuilt .jar file,  
or install clojure/leiningen and run `lein run` in `/desktop`.  

## Files / Directories

* `/minotaur-game-desktop-prebuilt-standalone.jar` (suprisingly) A prebuilt standalone java executable.
* `/desktop/game-rules.txt` The game rules.
* `/desktop/resources` Images, audio, and other files
* `/desktop/src` Desktop-specific code
* `/desktop/src-common` Cross-platform game code
